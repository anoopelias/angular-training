
function readFile(fileName) {
     //.. Read file and return a promise
}

function processFile(fileName) {
    return readFile(fileName).then((contents) => {
        let data = // Extract some information from contents;
        return asyncFoo(data.foo);
    }).then(fooInfo => {
        return asyncBar(fooInfo.bar);
    });
}

processFile('foo.txt').then(barInfo => {
    // readFile, asyncFoo & asyncBar is resolved by now.
    console.log(barInfo.qux);
});