const fs = require('fs');

function readFile(fileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, 'utf8', function(err, contents) {
            if (err) {
                reject(err);
            } else {
                resolve(contents);
            }
         });         
    })
}

readFile('foo.txt').then(contents => {
    console.log(contents);
});
