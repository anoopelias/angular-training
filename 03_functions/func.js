
function square(x) {
	return x * x;
}
console.log(square(9));

var cube = function(x) {
	return x * x * x;
};
console.log(cube(11));

var twice = (x) => {
	return 2 * x;
}
console.log(twice(9));

var thrice  = (x) => x * 3;
console.log(thrice(11));
