import { Observable, Subject } from 'rxjs/Rx';

let tick$ = Observable.interval(1000);
let subject = new Subject();

subject.subscribe(num => console.log(num));
subject.subscribe(num => console.log(num));

tick$.subscribe(subject);

