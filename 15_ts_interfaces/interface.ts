interface Control {
    render(): void;
}

interface SelectableControl extends Control {
    select(): void;
}

class Button implements Control {
    render() {
        // Render button
    }
}

class TextBox implements SelectableControl {
    render() {
        // Render textbox
    }

    select() {
        // Select textbox
    }

}