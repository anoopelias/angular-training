"use strict";
exports.__esModule = true;
var Rx_1 = require("rxjs/Rx");
var foo = Rx_1.Observable.create(function (observer) {
    console.log('Hello');
    observer.next(42);
});
foo.subscribe(function (x) {
    console.log(x);
});
foo.subscribe(function (y) {
    console.log(y);
});
