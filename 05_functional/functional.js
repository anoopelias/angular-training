
let arr = [2, 4, 6, 8, 10];

let filter_arr = arr.filter(n => n % 4 === 0); // [4, 8]

let map_arr = arr.map(n => n + 1); // [3, 5, 7, 9, 11]

let reduce_arr = arr.reduce((acc, n) => acc + n); // 30

console.log(filter_arr);
console.log(map_arr);
console.log(reduce_arr);