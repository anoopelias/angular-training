function superhero(target) {
    target.isSuperHero = true;
    target.power = 'flight';
}

@superhero
class MySuperHero {}


let batman = new MySuperHero();
console.log(batman.power);