
class Apple {
    constructor(type, color) {
        this.type = type;
        this.color = color;
    }

    getInfo() {
        return this.type + " " + this.color + " apple";
    }
}

let apple = new Apple("macintosh", "red");
console.log(apple.getInfo());