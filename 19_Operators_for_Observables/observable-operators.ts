import { Observable } from 'rxjs/Rx';

let obs = Observable.of(2, 4, 6, 8, 10, 12)
  .do(i => console.log(i));

console.log('------------')
obs.map(num => num * 2).subscribe(num => console.log('Mapped', num));

console.log('------------')
obs.filter(num => num % 4 === 0).subscribe(num => console.log('Filtered', num));

console.log('------------')
obs.reduce((acc, num) => num + acc).subscribe(num => console.log('Reduced', num));

console.log('------------')
obs.scan((acc, num) => num + acc).subscribe(num => console.log('Scanned', num));
